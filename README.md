# Btrfs compression test

Shell script to perform a simple compression test. The results are meant to
help with deciding on a suitable Btrfs compression algorithm for the system the
script is being run on.

**This is not a thorough benchmark**, because:

- Each compression algorithm is run only once
- Compression is performed on only a single file
- A lot of other reasons, too, I assume


## How to use it

The script expects to be run on a regular Fedora host. You can run it in a
container like this:

```
podman run --rm -it -v "$PWD:$PWD" -w "$PWD" --security-opt label=disable \
    registry.fedoraproject.org/fedora:37 ./btrfs_compression_test.sh
```

> You should be able to use `docker` instead of `podman`, too


## Contributing

If you want to add something to this script, feel free to open an issue or a
PR. This project currently doesn't strive to become a sophisticated Btrfs
compression benchmark, but this may change depending on other users interest.
